#include "DoliType.h"
#include "DoliIOCPWorker.h"
#include "DoliCompletionHandler.h"
#include "DoliNetworkSessionManager.h"
#include "DoliNetworkSession.h"

DoliCompletionHandler::DoliCompletionHandler()
{
	pp_IOCPWorkers_			= nullptr;
	pp_NetSessions_			= nullptr;

	h_HandleIOCP_			= 0;
	i32_WorkerCount_		= 0;

	pui64_RecvPacketCount	= nullptr;
	pui64_RecvPacketSize	= nullptr;
	pui64_SendPacketCount	= nullptr;
	pui64_SendPacketSize	= nullptr;

	p_SessionManager		= nullptr;
}

DoliCompletionHandler::~DoliCompletionHandler()
{
	OnDestroy();
}

bool DoliCompletionHandler::OnCreate(INT32 WorkCount, DoliNetworkSessionManager * pSessionManager)
{
	p_SessionManager = pSessionManager;

	// 1. Create IOCP
	h_HandleIOCP_ = ::CreateIoCompletionPort(INVALID_HANDLE_VALUE, 0, 0, WorkCount);
	if (h_HandleIOCP_ == 0)
	{
		// Create 실패 Log
		return false;
	}

	// 2. Create IOCP Worker
	i32_WorkerCount_ = WorkCount;
	pp_IOCPWorkers_ = (DoliIOCPWorker **)DoliMEM::DOLI_MEM_ALLOC(sizeof(DoliIOCPWorker*) * i32_WorkerCount_);
	UINT32	* pui32WorkerThreadIdList = (UINT32*)DoliMEM::DOLI_MEM_ALLOC(sizeof(UINT32) * i32_WorkerCount_);

	char TempStr[64];
	for (INT32 i = 0; i < i32_WorkerCount_; i++)
	{
		//void* mem = DoliMEM::DOLI_MEM_ALLOC(sizeof(DoliIOCPWorker));
		//pp_IOCPWorkers_[i] = new DoliIOCPWorker();
		pp_IOCPWorkers_[i] = DOLI_NEW_OBJECT(DoliIOCPWorker);
		pp_IOCPWorkers_[i]->SetIocpHandle(h_HandleIOCP_, p_SessionManager, i, this);

		sprintf_s(TempStr, "IOCPWOCKER%d", i);
		pp_IOCPWorkers_[i]->OnCreate(TempStr, 0, 4096, 0); // 이부분은 추후 워크에 같이 넣는 방식으로
		pui32WorkerThreadIdList[i] = pp_IOCPWorkers_[i]->GetThreadID();
	}
	p_SessionManager->SetWorkerThreadIDList(pui32WorkerThreadIdList);

	pp_NetSessions_ = (DoliNetworkSession **)DoliMEM::DOLI_MEM_ALLOC(sizeof(DoliNetworkSession*) * i32_WorkerCount_);
	DoliMEM::DOLI_FillZERO(pp_NetSessions_, sizeof(DoliNetworkSession*) * i32_WorkerCount_);

	pui64_RecvPacketCount = (UINT64*)DoliMEM::DOLI_MEM_ALLOC(sizeof(UINT64)*i32_WorkerCount_);
	if (NULL == pui64_RecvPacketCount)				return false;
	pui64_RecvPacketSize = (UINT64*)DoliMEM::DOLI_MEM_ALLOC(sizeof(UINT64)*i32_WorkerCount_);
	if (NULL == pui64_RecvPacketSize)				return false;

	pui64_SendPacketCount = (UINT64*)DoliMEM::DOLI_MEM_ALLOC(sizeof(UINT64)*i32_WorkerCount_);
	if (NULL == pui64_SendPacketCount)				return false;
	pui64_SendPacketSize = (UINT64*)DoliMEM::DOLI_MEM_ALLOC(sizeof(UINT64)*i32_WorkerCount_);
	if (NULL == pui64_SendPacketSize)				return false;

	return false;
}

bool DoliCompletionHandler::OnDestroy(void)
{
	DOLI_SAFE_FREE(pui64_RecvPacketCount);
	DOLI_SAFE_FREE(pui64_RecvPacketSize);
	DOLI_SAFE_FREE(pui64_SendPacketCount);
	DOLI_SAFE_FREE(pui64_SendPacketSize);

	if (pp_IOCPWorkers_ != NULL)
	{
		for (INT32 i = 0; i < i32_WorkerCount_; i++)
		{
			pp_IOCPWorkers_[i]->Terminate(); // 이부분은 추후 워크에 같이 넣는 방식으로
			DOLI_SAFE_RELEASE(DoliIOCPWorker, pp_IOCPWorkers_[i]);
		}

		DOLI_SAFE_FREE(pp_IOCPWorkers_);
	}
	DOLI_SAFE_FREE(pp_NetSessions_);

	return false;
}

void DoliCompletionHandler::OnAddIOCP(HANDLE handle, ULONG_PTR keyValue)
{
	::CreateIoCompletionPort(handle, h_HandleIOCP_, keyValue, 0);
}

bool DoliCompletionHandler::AddWorkingSession(DoliNetworkSession * pSession, INT32 Idx)
{
	bool Rv = true;
	dm_Mutex_.Enter();
	{
		for (INT32 i = 0; i < i32_WorkerCount_; i++)
		{
			if (pp_NetSessions_[i] == pSession)
			{
				Rv = false;
				break;
			}
		}
		if (Rv) pp_NetSessions_[Idx] = pSession;
	}
	dm_Mutex_.Leave();

	return Rv;
}

void DoliCompletionHandler::GetPacketInfo(UINT64 * pi64RecvCount, UINT64 * pi64RecvSize, UINT64 * pi64SendCount, UINT64 * pi64SendSize)
{
	*pi64RecvCount	= 0;
	*pi64RecvSize	= 0;
	*pi64SendCount	= 0;
	*pi64SendSize	= 0;

	for (INT32 i = 0; i < i32_WorkerCount_; i++)
	{
		*pi64RecvCount	+= pui64_RecvPacketCount[i];
		*pi64RecvSize	+= pui64_RecvPacketSize[i];
		*pi64SendCount	+= pui64_SendPacketCount[i];
		*pi64SendSize	+= pui64_SendPacketSize[i];
	}
}

void DoliCompletionHandler::GetPacketInfoThread(INT32 i32ThreadIdx, UINT64 * pi64RecvCount, UINT64 * pi64RecvSize, UINT64 * pi64SendCount, UINT64 * pi64SendSize)
{
	if (0 > i32ThreadIdx)					return;
	if (i32_WorkerCount_ <= i32ThreadIdx)	return;

	*pi64RecvCount	+= pui64_RecvPacketCount[i32ThreadIdx];
	*pi64RecvSize	+= pui64_RecvPacketSize[i32ThreadIdx];
	*pi64SendCount	+= pui64_SendPacketCount[i32ThreadIdx];
	*pi64SendSize	+= pui64_SendPacketSize[i32ThreadIdx];
}
