#pragma once

#include "DoliMutex.h"

class DoliIOCPWorker;
class DoliNetworkSession;
class DoliNetworkSessionManager;

class DOLI_EXPORT DoliCompletionHandler //: public DOLI_AFREE<DoliCompletionHandler>
{
public:
	DoliCompletionHandler();
	virtual ~DoliCompletionHandler();

public:
	bool OnCreate(INT32 WorkCount, DoliNetworkSessionManager * pSessionManager);
	bool OnDestroy(void);

	void	OnAddIOCP(HANDLE handle, ULONG_PTR keyValue);
	HANDLE	GetIOCPHandle(void)			{ return h_HandleIOCP_; }

	bool	AddWorkingSession(DoliNetworkSession *	pSession, INT32 Idx);

	void AddPacketInfo(INT32 Idx, INT32 i32PacketSize)
	{
		pui64_RecvPacketCount[Idx]++;
		pui64_RecvPacketSize[Idx] += i32PacketSize;
	}

	void AddSendPacketInfo(INT32 Idx, INT32 i32PacketSize)
	{
		pui64_SendPacketCount[Idx]++;
		pui64_SendPacketSize[Idx] += i32PacketSize;
	}

	void RemoveWorkingSession(INT32 Idx)
	{
		pp_NetSessions_[Idx] = NULL;
	}

	void	GetPacketInfo(UINT64* pi64Count, UINT64* pi64Size, UINT64* pi64SendCount, UINT64* pi64SendSize);
	void	GetPacketInfoThread(INT32 i32ThreadIdx, UINT64* pi64Count, UINT64* pi64Size, UINT64* pi64SendCount, UINT64* pi64SendSize);

	inline	DoliIOCPWorker*	GetIOCPWorker(UINT32 idx) { return pp_IOCPWorkers_[idx]; }

private:
	DoliIOCPWorker **		pp_IOCPWorkers_;
	DoliNetworkSession **	pp_NetSessions_;
	DoliMutex				dm_Mutex_;

	HANDLE					h_HandleIOCP_;
	INT32					i32_WorkerCount_;

	// RECV
	UINT64*					pui64_RecvPacketCount;
	UINT64*					pui64_RecvPacketSize;

	// SEND
	UINT64*					pui64_SendPacketCount;
	UINT64*					pui64_SendPacketSize;

	DoliNetworkSessionManager *	p_SessionManager;
};