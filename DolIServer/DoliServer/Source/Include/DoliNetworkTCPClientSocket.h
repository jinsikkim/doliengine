#pragma once

#define CLIENT_SOCKET_BUFFER_SIZE	(PACKETBUFFERSIZE * 3)

#define TCP_DISCONNECT_TYPE_OTHER			0X80000001
#define TCP_DISCONNECT_TYPE_READ_ERROR		0X80000002

typedef	INT32 (*CallbackRoutine)(void* pAgent, char* pPacket, INT32 i32Size);

class DoliNetworkPacket;

class DOLI_EXPORT DoliNetworkTCPClientSocket
{
public:
	DoliNetworkTCPClientSocket();
	~DoliNetworkTCPClientSocket();

public:
	bool					OnCreate(char* strIP, UINT16 ui16Port, CallbackRoutine CallBack = nullptr, void* pAgent = nullptr);
	bool					OnCreate(UINT32 ui32IP, UINT16 ui16Port, CallbackRoutine CallBack = nullptr, void* pAgent = nullptr);
	bool					SetSocket(SOCKET Socket);

	virtual INT32			OnReceive(void);
	virtual void			OnDestroy(void);
	virtual INT32			PacketParsing(char* pBuffer, INT32 nSize);
	virtual INT32			SendPacketMessage(DoliNetworkPacket* packet);
	virtual INT32			SendPacketMessage(const char* pBuffer, INT32 nSize);

	INT32					SelectEvent(void);
	bool					IsConnected(void) const;
	virtual void			DisConnect();

protected:
	SOCKET					GetSocket();
	char *					GetRecvBuffer();

private:
	bool					_Create(UINT32 ui32IP, UINT16 ui16Port, CallbackRoutine CallBack, void* pAgent);

private:
	SOCKET					s_socket_;
	WSAEVENT				h_recvEvent_;

	INT32					i32_receivedPacketSize_;
	char					ca_receiveBuffer_[CLIENT_SOCKET_BUFFER_SIZE];

	// for callback
	void*					p_Agent_;
	CallbackRoutine			pCB_CallbackFunc_;
};