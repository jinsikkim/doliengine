#pragma once

#include <stdlib.h>
#include <winsock2.h>

class ServerModule
{
public:
	ServerModule();
	~ServerModule();

public:
	bool	Create();
	void	Destroy();

	void	Start();

private:
	bool	_IPFileOpen();
	void	_SendMessage();

private:
	WSADATA			WsaData_;

	SOCKET			ServerSock_;
	SOCKADDR_IN		ServerAddr_;

	SOCKET			ClientSock_;
	SOCKADDR_IN		ClientAddr_;
};