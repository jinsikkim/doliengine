#include "DoliType.h"
#include "DoliNetworkSession.h"
#include "DoliNetworkSessionManager.h"

DoliNetworkSessionManager::DoliNetworkSessionManager()
{
	i32_NewSessionIdx_			= 0;
	i32_ActiveSessionCount_		= 0;
	
	pp_NetworkSessionList_		= nullptr;
	ui32_WorkerThreadIDList_	= nullptr;

	i32_MaxSessionIdx_			= 0;
}

DoliNetworkSessionManager::~DoliNetworkSessionManager()
{
}

bool DoliNetworkSessionManager::OnCreate(std::list<DoliNetworkSession*> list)
{
	i32_MaxSessionIdx_ = static_cast<INT32>(list.size());

	pp_NetworkSessionList_ = (DoliNetworkSession**)DoliMEM::DOLI_MEM_ALLOC(sizeof(DoliNetworkSession*) * i32_MaxSessionIdx_);

	std::list<DoliNetworkSession*>::iterator itor = list.begin();
	for (INT32 i = 0; i < i32_MaxSessionIdx_; i++)
	{
		pp_NetworkSessionList_[i] = (*itor);
		pp_NetworkSessionList_[i]->i32_SessionIdx_ = i;
		
		// Iterator 를 사용하려면 증가 시켜야 합니다.
		itor++;
	}

	return true;
}

void DoliNetworkSessionManager::OnDestory()
{
	DOLI_SAFE_FREE(pp_NetworkSessionList_);
	DOLI_SAFE_FREE(ui32_WorkerThreadIDList_);
}

void DoliNetworkSessionManager::OnUpdate()
{
	return;
}

ULONG_PTR DoliNetworkSessionManager::ConnectSession(SOCKET Socket, sockaddr_in * pAddr)
{
	DoliNetworkSession * p_NetworkSession = nullptr;
	dm_Mutex_.Enter();
	{
		p_NetworkSession = FindNewSession();
		if (p_NetworkSession != nullptr)
		{
			if (p_NetworkSession->OnConnect(Socket, pAddr))
			{
				i32_ActiveSessionCount_++;
			}
		}
	}
	dm_Mutex_.Leave();
	return (ULONG_PTR)p_NetworkSession;
}

void DoliNetworkSessionManager::DisConnectSession(DoliNetworkSession * pSession, bool bForceMain)
{
	dm_Mutex_.Enter();
	{
		if (pSession->GetIsActive() == true)
		{
			if (pSession->OnDisconnect(bForceMain) == true)
			{
				i32_ActiveSessionCount_--;
			}
		}
	}
	dm_Mutex_.Leave();
}

void DoliNetworkSessionManager::DisConnectAllSession(void)
{
	for (INT32 i = 0; i < i32_MaxSessionIdx_; i++ )
	{
		DisConnectSession(pp_NetworkSessionList_[i], true);
	}
	return;
}

DoliNetworkSession * DoliNetworkSessionManager::FindNewSession()
{
	DoliNetworkSession * p_NetworkSession = nullptr;
	for (INT32 i = 0; i < i32_MaxSessionIdx_; i++)
	{
		p_NetworkSession = pp_NetworkSessionList_[i32_NewSessionIdx_];
		if (p_NetworkSession->GetIsActive() == false)
		{
			i32_NewSessionIdx_++;
			if (i32_NewSessionIdx_ == i32_MaxSessionIdx_) i32_NewSessionIdx_ = 0;

			return p_NetworkSession;
		}
		i32_NewSessionIdx_++;
		if (i32_NewSessionIdx_ == i32_MaxSessionIdx_) i32_NewSessionIdx_ = 0;
	}
	return p_NetworkSession;
}
