#pragma once
#include <string>
#include <vector>
//#include "DoliMacro.h"

// Config 파서는 조금 더 구조를 생각해서 만들자
// Ini Parser를 따서 만들려니 여러가지를 같이 만들어야 한다.
// File을 미리 전부 읽어 테이블로 만들어 둔다.

#define	INI_SECTION_MAX		20
#define INI_SECTION_SIZE	128
#define INI_LINE_MAXSIZE	256

class INIValue
{
	friend class DoliConfigParser;

public:
	INIValue();
	~INIValue();

private:
	std::string	ValueName;
	std::string Value;
};

struct INISectionInfo
{
	std::vector<INIValue*>* ValueList;
	char				  Section[INI_SECTION_SIZE];
};

class DOLI_EXPORT DoliConfigParser
//class DoliConfigParser
{
public:
	DoliConfigParser();
	~DoliConfigParser();

	bool	OpenFile(const char * fileName);

	void	Destory();

public:
	// 여기서 Section을 가져온다.
	bool	GetSection(std::string FindSectionName);

	// Section을 가져와야 쓸 수 있음.
	// Get 시리즈 생성 필요
	bool GetINT32(std::string name, INT32 * value);
	bool GetUINT32(std::string name, UINT32 * value);

	bool GetREAL32(std::string name, REAL32 * value);
	bool GetREAL64(std::string name, REAL64 * value);

	bool GetString(std::string name, std::string& value);

private:
	void	_LineParsing(std::string& strDest, const char * Line);
	INIValue * _GetINIValue(std::string& name);

private:
	//std::list<INISectionInfo*> li_SectionList_;
	INISectionInfo*			li_SectionList_[INI_SECTION_MAX];
	INT32					i32_SectionSize_;

	bool b_isSection_;
	bool b_isValue_;

	INISectionInfo * p_TempSection_;
	INIValue * p_TempValue_;
};