#include "DoliType.h"
#include "DoliConfigParser.h"
#include "DoliMacro.h"
#include <fstream>

INIValue::INIValue()
{
}

INIValue::~INIValue()
{
}

// 여기는 Config Parser

DoliConfigParser::DoliConfigParser()
{
	b_isSection_ = false;
	b_isValue_ = false;
	p_TempValue_ = nullptr;
	i32_SectionSize_ = 0;
	
	DoliMEM::DOLI_FillZERO(li_SectionList_, sizeof(INISectionInfo*) * INI_SECTION_MAX);
}

DoliConfigParser::~DoliConfigParser()
{
	Destory();
}

bool DoliConfigParser::OpenFile(const char * fileName)
{
	std::ifstream file;
	
	file.open(fileName);
	if (!file)
	{
		// file 오픈 실패 로그 추가
		return false;
	}

	b_isSection_ = false;
	// 라인 한 줄 한 줄 읽읍시다.
	while (file.eof() == false)
	{
		char str[INI_LINE_MAXSIZE] = { 0, };

		// 한 줄 읽기
		file.getline(str, INI_LINE_MAXSIZE);
		
		// Line 파싱
		std::string Line;
		_LineParsing(Line, str);

		// Section 일 경우
		if (b_isSection_ == true)
		{
			b_isSection_ = false;
			p_TempSection_ = nullptr;

			// Section 구조체 생성
			p_TempSection_ = (INISectionInfo*)DoliMEM::DOLI_MEM_ALLOC(sizeof( INISectionInfo) );
			if (p_TempSection_ == nullptr) return false;

			DoliMEM::DOLI_FillZERO(p_TempSection_, sizeof(INISectionInfo));

			// Value List 생성
			std::vector<INIValue*>* valueList = new std::vector<INIValue*>();

			// Section Value List 등록
			p_TempSection_->ValueList = valueList;

			// Name 복사
			strcpy_s(p_TempSection_->Section, Line.c_str());

			// 실제 Section List에 등록
			li_SectionList_[i32_SectionSize_] = p_TempSection_;
			i32_SectionSize_++;
			//li_SectionList_.push_back(p_TempSection_);
		}
		// Section과 Value는 동시에 있을 수 없다.
		else if (b_isValue_ == true)
		{
			// Value 넣어주기.
			p_TempValue_->Value = Line;

			// List 등록
			p_TempSection_->ValueList->push_back(p_TempValue_);

			p_TempValue_ = nullptr;
			b_isValue_ = false;
		}
	}

	p_TempSection_ = nullptr;
	p_TempValue_ = nullptr;

	return true;
}

void DoliConfigParser::Destory()
{
	//for (INT32 i = 0; i < li_SectionList_.size(); i++)
	for (INT32 i = 0; i < i32_SectionSize_; i++)
	{
		//// 뒤에꺼 빼오기.
		//std::list<INISectionInfo*>::iterator itor = li_SectionList_.begin();
		//for (INT32 at = 0; at < i; at++) itor++;

		//// 빼온다.
		//p_TempSection_ = *(itor);
		p_TempSection_ = li_SectionList_[i];

		for (INT32 list = 0; list < p_TempSection_->ValueList->size(); list++)
		{
			//std::list<INIValue*>::iterator valueItor = p_TempSection_->ValueList->begin();
			//for (INT32 at = 0; at < list; at++) valueItor++;

			// Value를 빼온다.
			// Vector를 쓸까...
			p_TempValue_ = p_TempSection_->ValueList->at(list);

			DOLI_SAFE_DELETE(p_TempValue_);
		}

		DOLI_SAFE_FREE(p_TempSection_);
	}

	DoliMEM::DOLI_FillZERO(li_SectionList_, sizeof(INISectionInfo*) * INI_SECTION_MAX);
}

bool  DoliConfigParser::GetSection(std::string FindSectionName)
{
	for (INT32 i = 0; i < i32_SectionSize_; i++)
	{
		std::string Section = li_SectionList_[i]->Section;
		if (Section == FindSectionName)
		{
			p_TempSection_ = li_SectionList_[i];
			return true;
		}
	}

	return false;
}

bool DoliConfigParser::GetINT32(std::string name, INT32 * value)
{
	if (p_TempSection_ == nullptr) false;
	INIValue * inivalue = _GetINIValue(name);

	// null 검사
	if (inivalue == nullptr) return false;

	*(value) = std::stoi(inivalue->Value);

	return true;
}

bool DoliConfigParser::GetUINT32(std::string name, UINT32 * value)
{
	if (p_TempSection_ == nullptr) false;

	INIValue * inivalue = _GetINIValue(name);

	// null 검사
	if (inivalue == nullptr) return false;

	*(value) = std::stoi(inivalue->Value);

	return true;
}

bool DoliConfigParser::GetREAL32(std::string name, REAL32 * value)
{
	if (p_TempSection_ == nullptr) false;

	INIValue * inivalue = _GetINIValue(name);

	// null 검사
	if (inivalue == nullptr) return false;

	*(value) = std::stof(inivalue->Value);

	return true;
}

bool DoliConfigParser::GetREAL64(std::string name, REAL64 * value)
{
	if (p_TempSection_ == nullptr) false;

	INIValue * inivalue = _GetINIValue(name);

	// null 검사
	if (inivalue == nullptr) return false;

	*(value) = std::stod(inivalue->Value);

	return true;
}

bool DoliConfigParser::GetString(std::string name, std::string& value)
{
	if (p_TempSection_ == nullptr) false;

	INIValue * inivalue = _GetINIValue(name);

	// null 검사
	if (inivalue == nullptr) return false;

	value = inivalue->Value;

	return true;
}

void DoliConfigParser::_LineParsing(std::string& strDest, const char * Line)
{
	INT32 Len = (INT32)strlen(Line);
	
	char c_Text[INI_LINE_MAXSIZE] = { 0, };
	INT32 i32_cur = 0;
	bool b_section = false;
	bool b_Exit = false;

	// 라인 검사
	for (INT32 i = 0; i < Len; i++)
	{
		// Copy 여부
		bool b_copy = true;

		// 문자 하나 검사
		switch (Line[i])
		{
			// 섹션 구분
			// 섹션 시작
		case '[':
			{
				b_section = true;
				b_copy = false;
			}
			break;
			// 섹션 종료
		case ']':
			{
				if (b_section == true)
				{
					b_isSection_ = true;
					b_copy = false;
				}
			}
			break;
			// White Space
		case ' ':
		case '\t':
		case '\n':
		case '\r':
			{
				// 섹션 안쪽 내용이 아닐 경우에는 복사를 하지 않는다.
				if (b_section == false)
				{
					b_copy = false;
				}
			}
			break;
			// 주석 문자
		case ';':
			{
				// 섹션 안쪽 문자가 아니면 나간다.
				if (b_section == false)
				{
					b_Exit = true;
				}
			}
			break;
		case '=':
			{
				// = 이 들어가 있으면 Value입니다.
				if (b_isValue_ == false)
				{
					b_copy = false;
					b_isValue_ = true;

					// Value를 만들어 줍니다.
					p_TempValue_ = new INIValue();
					
					// 이름을 박아 넣는다.
					p_TempValue_->ValueName = c_Text;

					// 그리고 초기화 시킨다.
					i32_cur = 0;

					// 초기화
					DoliMEM::DOLI_FillZERO(c_Text, sizeof(char) * INI_LINE_MAXSIZE);
				}				
			}
			break;
		}
		
		if (b_Exit == true) break;

		if (b_copy == true)
		{
			c_Text[i32_cur++] = Line[i];
		}
	}

	c_Text[i32_cur] = 0;

	// String 변수에 넣어둡니다.
	strDest = c_Text;
}

INIValue * DoliConfigParser::_GetINIValue(std::string & name)
{
	if (p_TempSection_ == nullptr) return nullptr;

	const INT32 valuesize = (INT32)p_TempSection_->ValueList->size();
	for (INT32 i = 0; i < valuesize; i++)
	{
		INIValue * value = p_TempSection_->ValueList->at(i);
		if (value == nullptr) return nullptr;

		// 같은거 찾기.
		if (name == value->ValueName) return value;
	}

	return nullptr;
}
