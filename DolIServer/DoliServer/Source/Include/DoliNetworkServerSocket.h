#pragma once

#include "DoliMacro.h"

class DOLI_EXPORT DoliNetworkServerSocket
{
public:
	DoliNetworkServerSocket();
	~DoliNetworkServerSocket();

public:
	virtual bool	OnCreate(UINT32 ipNO, UINT16 portNO, int backlogCount = SOMAXCONN);
	virtual void	OnDestory();

	SOCKET			AcceptConnection(struct timeval * ptimeout, struct sockaddr_in * pAddr = NULL, INT32 * pSize = NULL);
	SOCKET			getListenSocket(void) { return socket_; }

private:
	SOCKET	socket_;
};