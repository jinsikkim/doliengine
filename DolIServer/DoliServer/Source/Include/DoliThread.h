#pragma once
#include "DoliMacro.h"

class DOLI_EXPORT DoliThread //: public DOLI_AFREE<T>
{
public:
	DoliThread();
	virtual ~DoliThread();

public:
	bool OnCreate(char * pName, UINT32 iAttr = 0, UINT32 iStackSize = 4096, void * UserData = nullptr);

	virtual bool	Suspend(void);

	virtual bool	Resume(void);
	
	virtual bool	Terminate(bool bWait = false);

	virtual UINT32	WaitForFinish(void);

	virtual void	BeforRunning(void* UserData);
	virtual UINT32	OnRunning(void* UserData) { UserData; return 0; }
	virtual void	AfterRunning(void* UserData);

public:
	void*	GetParameter(void)	{ return p_Parameter_; }

	UINT32	GetThreadID()		{ return ui32_ThreadID_; }
public:
	UINT32 ui_ReturnCode_;

protected:
	HANDLE h_Handle_;
	UINT32 ui32_ThreadID_;

	static DoliThread * pt_DoliThread_;// = nullptr;
	DoliThread * pt_Next_;

	void *	p_Parameter_;
	UINT32	ui32_Attribute_;
	UINT32  ui32_StackSize_;

	char	c_Name[64];
};