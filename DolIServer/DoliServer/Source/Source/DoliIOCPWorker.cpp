#include "DoliType.h"
#include "DoliIOCPWorker.h"
#include "DoliNetworkSession.h"
#include "DoliCompletionHandler.h"
#include "DoliNetworkSessionManager.h"

DoliIOCPWorker::DoliIOCPWorker()
{
	b_IsRun_				= false;
	i32_Idx_				= -1;
	p_SessionManager_		= nullptr;
	pp_WorkSession_			= nullptr;
	p_CompletionHandler_	= nullptr;
}

DoliIOCPWorker::~DoliIOCPWorker()
{
	b_IsRun_ = false;
	DoliThread::WaitForFinish();
}

void DoliIOCPWorker::BeforRunning(void * UserData)
{
	UserData;
	return;
}

UINT32 DoliIOCPWorker::OnRunning(void * UserData)
{
	UserData;
	b_IsRun_ = TRUE;
	ULONG_PTR IoKey;
	DWORD TransferSize;
	LPOVERLAPPED OverLapped;
	BOOL Rv;
	DoliNetworkSession * pSession;

	OVERLAPPED2 * poverlapped;
	
	// 실제 Work의 동작 루프
	while (b_IsRun_)
	{
		//Check IOPort 
		IoKey = 0;
		OverLapped = 0;

		// IOCP 처리 요청이 있는가 체크
		Rv = ::GetQueuedCompletionStatus(ph_IOCP_, &TransferSize, &IoKey, &OverLapped, 1);

		// ioKey로 Session이 넘어 옵니다.
		// 실제 IOCP 행동이 존재 할 경우에

		if (Rv && (IoKey != 0) && (TransferSize != 0) && (OverLapped != 0))
		{
			poverlapped = (OVERLAPPED2*)OverLapped;
			// Overlapped에서 Read 즉 Receive일 경우에 처리하는 행동
			if (SESSION_ASYNCFLAG_RECEIVE == poverlapped->flags)
			{
				//처리 
				pSession = (DoliNetworkSession*)IoKey;
				if (pSession->GetIsActive())
				{
					if (p_CompletionHandler_->AddWorkingSession(pSession, i32_Idx_))
					{
						pSession->i32_WorkThreadIdx_ = i32_Idx_;
						pSession->Dispatch(TransferSize);
						pSession->i32_WorkThreadIdx_ = -1;

						p_CompletionHandler_->AddPacketInfo(i32_Idx_, TransferSize);

						p_CompletionHandler_->RemoveWorkingSession(i32_Idx_);

						// Read Event 를 걸어줍니다.
						pSession->WaitPacketReceive(i32_Idx_);
					}
				}
			}
			// Send일 경우
			else if (SESSION_ASYNCFLAG_SEND == poverlapped->flags)
			{
				// Send 일때는 아무런 일도 하지 않습니다. 
				p_CompletionHandler_->AddSendPacketInfo(i32_Idx_, TransferSize);
			}

		}
		else
		{
			if (IoKey != 0 && GetLastError() != ERROR_OPERATION_ABORTED)
			{//이녀석은 삭제처리 합니다. 
				pSession = (DoliNetworkSession*)IoKey;
				if (p_CompletionHandler_->AddWorkingSession(pSession, i32_Idx_))
				{
					pSession->i32_WorkThreadIdx_ = i32_Idx_;
					p_SessionManager_->DisConnectSession(pSession, false);
					pSession->i32_WorkThreadIdx_ = -1;
					p_CompletionHandler_->RemoveWorkingSession(i32_Idx_);
				}
				else
				{
					// DisConnect Log
				}
			}
		}	
		::WaitForSingleObject(h_Handle_, 1);
	}

	return 0;
}

void DoliIOCPWorker::AfterRunning(void * UserData)
{
	UserData;
	return;
}
