#pragma once

#include "DoliMutex.h"
#include <list>

class DoliNetworkSession;

class DOLI_EXPORT DoliNetworkSessionManager //: public DOLI_AFREE<DoliNetworkSessionManager>
{
public:
	DoliNetworkSessionManager();
	virtual ~DoliNetworkSessionManager();

	virtual bool			OnCreate(std::list<DoliNetworkSession*> list);
	virtual void			OnDestory();

	virtual void			OnUpdate();

	virtual ULONG_PTR		ConnectSession(SOCKET Socket, struct sockaddr_in * pAddr);
	void					DisConnectSession(DoliNetworkSession * pSession, bool bForceMain);
	void					DisConnectAllSession(void);

	INT32					GetActiveSessionCount() { return i32_ActiveSessionCount_; }
	INT32					GetMaxSessionIndex()	{ return i32_MaxSessionIdx_; }

	UINT32*					GetWorkerThreadIDList() { return ui32_WorkerThreadIDList_; }
	void					SetWorkerThreadIDList(UINT32* WorkerThreadIDList) { ui32_WorkerThreadIDList_ = WorkerThreadIDList; }

protected:
	DoliNetworkSession*		FindNewSession();

private:
	INT32					i32_ActiveSessionCount_;
	DoliMutex				dm_Mutex_;

	DoliNetworkSession **	pp_NetworkSessionList_;
	INT32					i32_NewSessionIdx_;
	INT32					i32_MaxSessionIdx_;

	UINT32*					ui32_WorkerThreadIDList_;

};