#include "DoliType.h"
#include "DoliServerContext.h"
#include "DoliCompletionHandler.h"
#include "DoliNetworkSessionManager.h"
#include "DoliNetworkAcceptor.h"

DoliServerContext::DoliServerContext()
{
	p_Acceptor_				= nullptr;
	p_CompletionHandler_	= nullptr;
	p_SessionManager_		= nullptr;
}

DoliServerContext::~DoliServerContext()
{
	OnDestroy();
}

bool DoliServerContext::OnCreate(UINT8 SocketCount, UINT32 * pAddress, UINT16 * pPort, UINT8 * pTimeOut, INT32 WorkCount, DoliNetworkSessionManager * pSessionManager)
{
	// 1. Session Manager
	if (pSessionManager == nullptr)
	{
		// 세션 매니저가 없다. 로그 기록
		return false;
	}
	p_SessionManager_ = pSessionManager;

	// 2. Completion Handler
	p_CompletionHandler_ = DOLI_NEW_OBJECT(DoliCompletionHandler);
	if (p_CompletionHandler_->OnCreate(WorkCount, p_SessionManager_) == false)
	{
		// Competion Handler 추가 실패. 로그 기록
		return false;
	}

	// 3. Acceptor
	p_Acceptor_ = DOLI_NEW_OBJECT(DoliNetworkAcceptor);
	if (p_Acceptor_->OnCreate(SocketCount, pAddress, pPort, pTimeOut, p_SessionManager_, p_CompletionHandler_) == false)
	{
		// Acceptor 생성 실패. 로그 기록
		return false;
	}

	return true;
}

bool DoliServerContext::OnDestroy(void)
{
	DOLI_SAFE_RELEASE(DoliCompletionHandler, p_CompletionHandler_);
	DOLI_SAFE_RELEASE(DoliNetworkAcceptor, p_Acceptor_);
	p_SessionManager_ = nullptr;

	return true;
}

inline INT32 DoliServerContext::GetSessionCount(void)
{
	return p_SessionManager_->GetActiveSessionCount();
}

inline void DoliServerContext::GetPacketInfo(UINT64 * pi64Count, UINT64 * pi64Size, UINT64 * pi64SendCount, UINT64 * pi64SendSize)
{
	p_CompletionHandler_->GetPacketInfo(pi64Count, pi64Size, pi64SendCount, pi64SendSize);
}

inline void DoliServerContext::GetPacketInfoThread(INT32 i32ThreadIdx, UINT64 * pi64Count, UINT64 * pi64Size, UINT64 * pi64SendCount, UINT64 * pi64SendSize)
{
	p_CompletionHandler_->GetPacketInfoThread(i32ThreadIdx, pi64Count, pi64Size, pi64SendCount, pi64SendSize);
}
