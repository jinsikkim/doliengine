#include <iostream>	
#include <winsock2.h>

bool main()
{
	std::cout << "Client ������Ʈ" << std::endl;

	WSADATA wsaData;
	SOCKET hSocket;
	SOCKADDR_IN servAddr;

	if (WSAStartup(MAKEWORD(2, 2, ), &wsaData) != 0)
	{
		std::cout << "WSAStartup Error" << std::endl;
		return false;
	}

	hSocket = socket(PF_INET, SOCK_STREAM, 0);
	if (hSocket == INVALID_SOCKET)
	{
		std::cout << "socket Error" << std::endl;
		return false;
	}

	memset(&servAddr, 0, sizeof(servAddr));
	servAddr.sin_family = AF_INET;
	servAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servAddr.sin_port = htons(1214);

	if (connect(hSocket, (SOCKADDR*)&servAddr, sizeof(servAddr)) == SOCKET_ERROR)
	{
		std::cout << "connect Error" << std::endl;
		return false;
	}

	int strLen = 0;
	char message[255] = { 0, };
	strLen = recv(hSocket, message, sizeof(message) - 1, 0);
	if (strLen == -1)
	{
		std::cout << "recv Error" << std::endl;
		return false;
	}

	std::cout << "Server Recv Message : " << message << std::endl;
	closesocket(hSocket);
	WSACleanup();

	return true;
}