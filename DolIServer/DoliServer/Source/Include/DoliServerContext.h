#pragma once

class DoliNetworkAcceptor;
class DoliCompletionHandler;
class DoliNetworkSessionManager;

class DOLI_EXPORT DoliServerContext
{
public:
	DoliServerContext();
	~DoliServerContext();

	virtual bool				OnCreate(UINT8 SocketCount, UINT32 * pAddress, UINT16 * pPort, UINT8 * pTimeOut, INT32 WorkCount, DoliNetworkSessionManager * pSessionManager);
	virtual void				OnUpdate(INT32 Command) { Command; }
	virtual bool				OnDestroy(void);

	inline INT32				GetSessionCount(void);
	inline void					GetPacketInfo(UINT64* pi64Count, UINT64* pi64Size, UINT64* pi64SendCount, UINT64* pi64SendSize);
	inline void					GetPacketInfoThread(INT32 i32ThreadIdx, UINT64* pi64Count, UINT64* pi64Size, UINT64* pi64SendCount, UINT64* pi64SendSize);

	inline DoliCompletionHandler*	GetCompletionHandler() { return p_CompletionHandler_; }

protected:
	DoliNetworkAcceptor			* p_Acceptor_;
	DoliCompletionHandler		* p_CompletionHandler_;
	DoliNetworkSessionManager	* p_SessionManager_;
};