#pragma once

//#include "DoliAFree.h"

typedef CRITICAL_SECTION DOLI_MUTEX;

class DOLI_EXPORT DoliMutex //: public DOLI_AFREE<DoliMutex>
{
public:
	DoliMutex();
	virtual ~DoliMutex();

public:
	void	Enter();
	BOOL	TryEnter();
	void	Leave();

private:
	DOLI_MUTEX cs_mutex_;
};