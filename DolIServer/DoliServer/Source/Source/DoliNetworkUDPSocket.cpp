#include "DoliType.h"
#include "DoliNetworkUDPSocket.h"
#include "DoliNetFunction.h"

DoliNetworkUDPSocket::DoliNetworkUDPSocket()
{
	s_socket_ = INVALID_SOCKET;
	pc_ReciveBuffer_ = NULL;
	DoliMEM::DOLI_FillZERO(&s_sockaddr_, sizeof(sockaddr));

	i32_BufSize_ = 0;
}

DoliNetworkUDPSocket::~DoliNetworkUDPSocket()
{
	Destroy();
}

bool DoliNetworkUDPSocket::Create(INT32 nBufSize, UINT16 nBindPort)
{
	SOCKADDR_IN	localAddr;
	DoliMEM::DOLI_FillZERO(&localAddr, sizeof(localAddr));

	// 1. Socket 생성
	s_socket_ = ::socket(AF_INET, SOCK_DGRAM, 0);

	if (s_socket_ == INVALID_SOCKET)
	{
		// Socket 만들기 실패 로그 기록
		return false;
	}

	// 2. bind
	localAddr.sin_family = AF_INET;
	localAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	localAddr.sin_port = htons(nBindPort);

	if (::bind(s_socket_, (struct sockaddr *) &localAddr, sizeof(localAddr)) == SOCKET_ERROR)
	{
		// Bind 실패 로그 기록
		return false;
	}

	// 3. Buffer Create
	i32_BufSize_ = nBufSize;
	pc_ReciveBuffer_ = (char *)DoliMEM::DOLI_MEM_ALLOC(i32_BufSize_);
	DoliMEM::DOLI_FillZERO(pc_ReciveBuffer_, sizeof(i32_BufSize_));

	return true;
}

bool DoliNetworkUDPSocket::Destroy()
{
	INT32	result;

	// 1. socket 제거
	if (s_socket_ != INVALID_SOCKET)
	{
		result = ::shutdown(s_socket_, SD_BOTH);
		if (result < 0)
		{
			// shutdown 실패 로그 기록
			return false;
		}
		result = ::closesocket(s_socket_);
		if (result < 0)
		{
			// UDP Socket Close 실패 로그 기록
			return false;
		}

		s_socket_ = INVALID_SOCKET;
	}

	// 2. Buffer 제거
	DOLI_SAFE_FREE(pc_ReciveBuffer_);

	return true;
}

INT32 DoliNetworkUDPSocket::Send(char * pBuf, INT32 nSize, UINT32 nIP, UINT16 nPort)
{
	// 1. Socket 정보 생성
	INT32	nRv;
	SOCKADDR_IN cast;
	cast.sin_family = AF_INET;
	cast.sin_addr.s_addr = nIP;
	cast.sin_port = ::htons(nPort);

	if (s_socket_ == INVALID_SOCKET)
	{
		// Socket이 없다. 로그 기록
		return -1;
	}

	nRv = ::sendto(s_socket_, pBuf, nSize, 0, (const sockaddr *)&cast, sizeof(sockaddr));
	if (nRv < 0)
	{
		// Send 실패 로그 기록
	}

	return nRv;
}

INT32 DoliNetworkUDPSocket::Recv(void)
{
	INT32		len = sizeof(sockaddr);

	return ::recvfrom(s_socket_, pc_ReciveBuffer_, i32_BufSize_, 0, &s_sockaddr_, &len);
}
