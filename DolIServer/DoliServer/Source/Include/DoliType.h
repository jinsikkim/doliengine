#pragma once

// Lib파일을 만들기 위함.
//#define DOLI_EXPORT __declspec( dllexport)

// 이걸 선언해야 Windows와 winsock2의 충돌을 피할 수 있다.
#define WIN32_LEAN_AND_MEAN

// 이걸 선언해야 inet_addr이 에러가 안난다..
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#pragma warning( disable : 4995)
#pragma warning( disable : 4996)
#pragma warning( disable : 4251) 

#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <xmmintrin.h>

#include <wininet.h>
#include <WinSock2.h>

// Type 재정의
typedef unsigned char			UINT8;

typedef signed short int		INT16;
typedef unsigned short int		UINT16;

typedef signed int				INT32;
typedef unsigned int			UINT32;

typedef __int64					INT64;
typedef unsigned __int64		UINT64;

typedef float					REAL32;
typedef double					REAL64;

typedef WCHAR					WCHAR16;
typedef unsigned long			WCHAR32;

// Protocol Size
typedef					UINT16	PROTOCOL;

#define		PACKETBUFFERSIZE			8912
#define		ACCEPTOR_RECV_TIME			5
#define		PACKETHEADERSIZE			4
#define		PACKETDATASIZE				(PACKETBUFFERSIZE-PACKETHEADERSIZE)

#include "DoliMacro.h"
//#include "DoliAFree.h"