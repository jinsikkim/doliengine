//#pragma once
#ifndef _DOLI_MACRO_H_
#define _DOLI_MACRO_H_

#include <concrt.h>

// Lib파일을 만들기 위함.
#ifdef DOLI_DLL
	#define DOLI_EXPORT __declspec( dllexport)
#else
	#define DOLI_EXPORT __declspec( dllimport)
#endif

#define		DOLI_SAFE_FREE(P)			if(P!=NULL){ Concurrency::Free(P); P=NULL; }
#define		DOLI_SAFE_DELETE(P)			if(P!=NULL){ delete P; P=NULL; }

#define		DOLI_NEW_OBJECT(CName)				DoliMEM::NEW_OBJECT<CName>(sizeof(CName));
#define		DOLI_SAFE_RELEASE(CName,pointer)	DoliMEM::SAFE_RELEASE<CName>(pointer);

#define		DOLI_ANDBIT( a, b)							((a) & (b))

namespace DoliMEM
{
	inline
	void*	DOLI_MEM_ALLOC(size_t size)
	{
		return Concurrency::Alloc(size);
	}

	inline
	void	DOLI_FillZERO(void * pDest, UINT32 sz)
	{
		::SecureZeroMemory(pDest, sz);
	}

	inline
	void	DOLI_MEMCOPY(void * pDest, const void * pSrc, UINT32 sz)
	{
		::CopyMemory(pDest, pSrc, sz);
	}

	template<class T>
	T* NEW_OBJECT(size_t size)
	{
		T* _data = static_cast<T*>(Concurrency::Alloc(size));
		new(_data) T;
		return _data;
	}

	template<class T>
	void SAFE_RELEASE(T* p)
	{
		p->~T();
		DOLI_SAFE_FREE(p);
	}
}



DOLI_EXPORT	void DOLIPRINTF(const char *format, ...);
DOLI_EXPORT	void DOLIPRINTF(const WCHAR16 *format, ...);

#define		DOLITRACE		DOLIPRINTF

#endif