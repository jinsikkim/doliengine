#pragma once

class DOLI_EXPORT DoliNetworkUDPSocket
{
public:
	DoliNetworkUDPSocket();
	~DoliNetworkUDPSocket();

	bool	Create(INT32 nBufSize, UINT16 nBindPort);
	bool	Destroy();

	INT32	Send(char * pBuf, INT32 nSize, UINT32 nIP, UINT16 nPort);
	INT32	Recv(void);

	char *	getPacket(void)			{ return pc_ReciveBuffer_; }
	char *	getPacket(INT32 nPos)	{ return &pc_ReciveBuffer_[nPos]; }
	sockaddr * getRecvAddr(void)	{ return &s_sockaddr_; }

protected:
	SOCKET		s_socket_;
	sockaddr	s_sockaddr_;

	INT32		i32_BufSize_;
	char	*	pc_ReciveBuffer_;
};