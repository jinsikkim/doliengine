#pragma once

#include "DoliThread.h"

class DoliCompletionHandler;
class DoliNetworkSessionManager;
class DoliNetworkServerSocket;

class DOLI_EXPORT DoliNetworkAcceptor : public DoliThread
{
public:
	DoliNetworkAcceptor();
	virtual ~DoliNetworkAcceptor();

public:
	bool OnCreate(UINT8 SocketCount, UINT32 * pIpAddress, UINT16 * pPortNo, UINT8 * pTimeOut, DoliNetworkSessionManager * pSessionManager, DoliCompletionHandler * pIOCP);

	bool OnDestroy(void);

	virtual void	BeforRunning(void* UserData);
	virtual UINT32	OnRunning(void* UserData);
	virtual void	AfterRunning(void* UserData);

private:
	bool					b_IsRunning_;
	DoliCompletionHandler*	p_IOCP_;

protected:
	DoliNetworkSessionManager * p_SessionManager_;

	UINT8						ui8_SocketCount_;
	UINT8 *						pui8_TimeOut_;
	DoliNetworkServerSocket**	pp_ServerSocket_;
};