#pragma once

//#include "DoliAFree.h"
#include "DoliMacro.h"

class DoliNetworkPacket //: public DOLI_AFREE<DoliNetworkPacket>
{
public:
	DoliNetworkPacket();
	DoliNetworkPacket(PROTOCOL protocol);
	~DoliNetworkPacket();

public:
	void		Clear(void);
	void		SetProtocol(PROTOCOL ProtocolID) { *h_PacketHeader_.pProtocolID = ProtocolID; }
	INT32		GetPacketSize(void)			{ return (GetDataFieldSize() + PACKETHEADERSIZE); }
	INT32		GetReceivedSize(void)		{ return i32_Size_; }
	PROTOCOL	GetProtocolID(void)			{ return *h_PacketHeader_.pProtocolID; }
	UINT16		GetDataFieldSize(void)		{ return (*h_PacketHeader_.pDataSize) & 0x7FFF; }
	char	*	GetPacketBuffer(void)		{ return ca_PacketBuffer_; }
	char	*	GetFieldBuffer(void)		{ return cp_DataField_; }

	// Packet Read / Write
	void		CopyToBuffer(const char * buffer, INT32 size, INT32 StartPos = 0);
	void		ReadData(void* buffer, INT32 size);
	bool		WriteData(const void* buffer, UINT16 size);
	void		WriteData(UINT16 ui16Pos, const void* buffer, UINT16 size);

	// 암호화는 다음에 생각해봅시다.
//public:
//	BOOL		Encript(UINT32 c);
//	BOOL		Decript(UINT32 c);

private:
	typedef struct
	{
		UINT16 * pDataSize;
		UINT16 * pProtocolID;
	}PACKETHEADER;

	PACKETHEADER	h_PacketHeader_;
	char			ca_PacketBuffer_[PACKETBUFFERSIZE];
	char	*		cp_DataField_;
	char	*		cp_ReadPosition_;
	char	*		cp_WritePosition_;
	char	*		cp_EndOfDataField_;
	INT32			i32_Size_;
};