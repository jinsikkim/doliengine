#include "DoliType.h"
#include "DoliNetworkPacket.h"

DoliNetworkPacket::DoliNetworkPacket()
{
	Clear();
}

DoliNetworkPacket::DoliNetworkPacket(PROTOCOL protocol)
{
	Clear();
	SetProtocol(protocol);
}

DoliNetworkPacket::~DoliNetworkPacket()
{
}

void DoliNetworkPacket::Clear(void)
{
	DoliMEM::DOLI_FillZERO(ca_PacketBuffer_, PACKETBUFFERSIZE);

	h_PacketHeader_.pDataSize	= (UINT16 *)&ca_PacketBuffer_[0];	//  packetSize size = 2
	h_PacketHeader_.pProtocolID = (UINT16 *)&ca_PacketBuffer_[2];	//  protocolID size	= 2
	cp_DataField_				= &ca_PacketBuffer_[PACKETHEADERSIZE];
	cp_ReadPosition_			= cp_WritePosition_ = cp_DataField_;
	cp_EndOfDataField_			= &ca_PacketBuffer_[PACKETBUFFERSIZE];
	i32_Size_					= 0;
}

void DoliNetworkPacket::CopyToBuffer(const char * buffer, INT32 size, INT32 StartPos)
{
	if (size > PACKETBUFFERSIZE)
	{
		DOLITRACE("[DoliNetworkPacket::CopyToBuffer] Packet Size Error \n");
		return;
	}

	Clear();
	DoliMEM::DOLI_MEMCOPY(ca_PacketBuffer_, &buffer[StartPos], size);
	i32_Size_ = size;
}

void DoliNetworkPacket::ReadData(void * buffer, INT32 size)
{
	if (0 == size)
	{
		DOLITRACE("[DoliNetworkPacket::ReadData] Size Zero\n");
		return;
	}

	if (cp_ReadPosition_ + size > cp_EndOfDataField_)
	{
		DOLITRACE("[DoliNetworkPacket::ReadData] Error\n");
		return;
	}

	DoliMEM::DOLI_MEMCOPY(buffer, ca_PacketBuffer_, size);
	cp_ReadPosition_ += size;
}

bool DoliNetworkPacket::WriteData(const void * buffer, UINT16 size)
{
	if (0 == size)
	{
		DOLITRACE("[DoliNetworkPacket::WriteData] Size Zero\n");
		return FALSE;
	}

	if (cp_WritePosition_ + size > cp_EndOfDataField_)
	{
		DOLITRACE("[DoliNetworkPacket::WriteData] Write Failed \n");
		return FALSE;
	}

	DoliMEM::DOLI_MEMCOPY(cp_WritePosition_, buffer, size);
	cp_WritePosition_	+= size;
	i32_Size_			+= size;

	*h_PacketHeader_.pDataSize = (UINT16)i32_Size_;

	return TRUE;
}

void DoliNetworkPacket::WriteData(UINT16 ui16Pos, const void * buffer, UINT16 size)
{
	if (0 == size)	return;

	if (&ca_PacketBuffer_[PACKETHEADERSIZE + ui16Pos] + size > cp_EndOfDataField_)
	{
		DOLITRACE("[DoliNetworkPacket::WriteData] Write Failed \n");
		return;
	}

	UINT16 ui16NewSize = ui16Pos + size;
	if (i32_Size_ < ui16NewSize)
	{	// 데이터 사이즈 변경이 일어났다면 사이즈 증가시켜줍니다.		
		cp_WritePosition_ = &ca_PacketBuffer_[PACKETHEADERSIZE + ui16Pos];
		DoliMEM::DOLI_MEMCOPY(cp_WritePosition_, buffer, size);

		cp_WritePosition_	+= size;
		i32_Size_			= ui16NewSize;
	}
	else
	{
		DoliMEM::DOLI_MEMCOPY(&ca_PacketBuffer_[PACKETHEADERSIZE + ui16Pos], buffer, size);
	}
}
