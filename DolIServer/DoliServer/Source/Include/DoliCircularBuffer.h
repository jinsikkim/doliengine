#pragma once

// Buffer를 Circular 형태로 만든다.

enum CIRCULAR_BUFFER_RESULT
{
	CIRCULAR_BUFFER_SUCCESS					= 0x00000000,
	CIRCULAR_BUFFER_ERROR_BUFFER_FULLIDX	= 0x80000001,
	CIRCULAR_BUFFER_ERROR_BUFFER_FULL		= 0x80000002,
};

class DOLI_EXPORT DoliCircularBuffer
{
public:
	DoliCircularBuffer();
	~DoliCircularBuffer();

public:
	bool					Create(INT32 i32BufferSize, INT32 i32BufferCount);
	void					Destroy(void);

	INT32					GetEmptyBufferCount(void);							// 남은 버퍼의 카운트를 전달 한다. 
	INT32					GetBufferCount(void);								// 사용하는 버퍼의 카운트를 전달한다. 

	// Push																				
	INT32					Push(void * pBuffer);								// Buffer를 바로 기록 하여 index를 빼온다.

	// Push Pointer로 Buffer에 기록 후
	// PushPointerIdx로 Write Index를 증가 시킨다.
	void*					PushPointer(void);									// 기록할 buffer의 포인터
	void					PushPointerIdx(void);								// Buffer를 기록 완료하여 다음 index로 넘어간다.
	
	// Pop
	// Pop으로 Buffer를 가져와서 사용 후
	// 사용 다 했다는 Index를 PopIdx로 증가시킨다.
	void *					Pop(void);											// 뽑아 올 Buffer의 포인터
	void					PopIdx(void);										// 뽑아 올 Buffer Index

private:
	UINT32					ui32_BufferSize_;
	UINT32					ui32_BufferCount_;

	UINT32					ui32_WriteIdx_;
	UINT32					ui32_ReadIdx_;

	char*					cp_Buffer_;
};