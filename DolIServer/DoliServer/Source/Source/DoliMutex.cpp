#include "DoliType.h"
#include "DoliMutex.h"

DoliMutex::DoliMutex()
{
	// Critical Section 초기화
	::InitializeCriticalSection(&cs_mutex_);
}

DoliMutex::~DoliMutex()
{
	// Critical Section 제거
	::DeleteCriticalSection(&cs_mutex_);
}

void DoliMutex::Enter()
{
	// Critical Section 진입
	::EnterCriticalSection(&cs_mutex_);
}

BOOL DoliMutex::TryEnter()
{
	// 재시도
	return ::TryEnterCriticalSection(&cs_mutex_);
}

void DoliMutex::Leave()
{
	// Critical Section 나가기
	::LeaveCriticalSection(&cs_mutex_);
}
