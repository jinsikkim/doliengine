#include "DoliType.h"
#include "DoliNetworkAcceptor.h"
#include "DoliNetworkServerSocket.h"
#include "DoliNetworkSessionManager.h"
#include "DoliCompletionHandler.h"
#include "DoliNetworkSession.h"

DoliNetworkAcceptor::DoliNetworkAcceptor()
{
	b_IsRunning_		= false;
	pp_ServerSocket_	= nullptr;
	p_SessionManager_	= nullptr;
	p_IOCP_				= nullptr;

	ui8_SocketCount_	= 0;
	pui8_TimeOut_		= nullptr;
}

DoliNetworkAcceptor::~DoliNetworkAcceptor()
{
	b_IsRunning_ = false;
	DoliThread::WaitForFinish();

	for (UINT8 i = 0; i < ui8_SocketCount_; i++) 
		DOLI_SAFE_RELEASE(DoliNetworkServerSocket, pp_ServerSocket_[i]);

	DOLI_SAFE_FREE(pui8_TimeOut_);
	DOLI_SAFE_FREE(pp_ServerSocket_);
}

bool DoliNetworkAcceptor::OnCreate(UINT8 SocketCount, UINT32 * pIpAddress, UINT16 * pPortNo, UINT8 * pTimeOut, DoliNetworkSessionManager * pSessionManager, DoliCompletionHandler * pIOCP)
{
	// 1. Set Value 
	p_SessionManager_	= pSessionManager;
	p_IOCP_				= pIOCP;
	ui8_SocketCount_	= SocketCount;

	// 2. Create Server Socket 
	pp_ServerSocket_ = (DoliNetworkServerSocket**)DoliMEM::DOLI_MEM_ALLOC(sizeof(DoliNetworkServerSocket*) * ui8_SocketCount_);
	pui8_TimeOut_ = (UINT8*)DoliMEM::DOLI_MEM_ALLOC(sizeof(UINT8) * ui8_SocketCount_);
	for (UINT8 i = 0; i < ui8_SocketCount_; i++)
	{
		pp_ServerSocket_[i] = DOLI_NEW_OBJECT(DoliNetworkServerSocket);
		if (!pp_ServerSocket_[i]->OnCreate(pIpAddress[i], pPortNo[i]))
			return false;

		pui8_TimeOut_[i] = pTimeOut[i];
	}

	// 3. Create Thread 
	DoliThread::OnCreate("DoliAcceptor", 0, 4096, NULL);

	return true;
}

bool DoliNetworkAcceptor::OnDestroy(void)
{
	return true;
}

void DoliNetworkAcceptor::BeforRunning(void * UserData)
{
	UserData;
	return;
}

UINT32 DoliNetworkAcceptor::OnRunning(void * UserData)
{
	UserData;
	struct		timeval timeout;
	SOCKET		SocketAccept;
	ULONG_PTR	SessionPointer;

	// 1. Start
	b_IsRunning_ = true;

	struct sockaddr_in FromAddr;
	INT32  AddrSize;
	while (b_IsRunning_)
	{
		for (UINT8 i = 0; i < ui8_SocketCount_; i++)
		{
			timeout.tv_sec = 0;
			timeout.tv_usec = pui8_TimeOut_[i];

			AddrSize = sizeof(sockaddr_in);
			SocketAccept = pp_ServerSocket_[i]->AcceptConnection(&timeout, &FromAddr, &AddrSize);

			//정상적인 소켓 입니다. 처리를 합니다. 
			if (SocketAccept != INVALID_SOCKET)
			{
				SessionPointer = p_SessionManager_->ConnectSession(SocketAccept, &FromAddr);

				if (SessionPointer != 0)
				{
					p_IOCP_->OnAddIOCP((HANDLE)SocketAccept, SessionPointer);
					((DoliNetworkSession*)SessionPointer)->OnConnectInit();
				}
				else
				{
					// 허용 세션 초과 시, 연결을 끊는다.
					::shutdown(SocketAccept, SD_BOTH);
					::closesocket(SocketAccept);
				}
			}
		}
	}

	return UINT32();
}

void DoliNetworkAcceptor::AfterRunning(void * UserData)
{
	UserData;
	return;
}
