#include "pch.h"
#include "ServerModule.h"
#include "Defined.h"
#include "DoliConfigParser.h"

void Config()
{
	DoliConfigParser Parser;

	Parser.OpenFile("Config.ini");

	if (Parser.GetSection("Section1") == true)
	{
		INT32 value = 0;
		Parser.GetINT32("value1", &value);

		std::cout << value << std::endl;
	}
}

bool main()
{
	Config();

	// 서버 모듈 생성
	ServerModule * pServer = new ServerModule();

	// Create
	if (pServer->Create() == false)
	{
		pServer->Destroy();

		SAFE_DELETE(pServer);
		return false;
	}

	pServer->Start();

	pServer->Destroy();
	SAFE_DELETE(pServer);

	return true;
}