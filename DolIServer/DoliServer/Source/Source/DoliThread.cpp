#include "DoliType.h"
#include "DoliThread.h"

DoliThread * DoliThread::pt_DoliThread_ = NULL;

// 기본적인 스레드 루트 함수.
// 루프가 없기때문에 그냥 멈춘다.
static UINT32 _DefaultThreadFunc(DoliThread * pThread)
{
	UINT32 Rv;

	pThread->BeforRunning(pThread->GetParameter());

	Rv = pThread->OnRunning(pThread->GetParameter());
	pThread->ui_ReturnCode_ = Rv;

	pThread->AfterRunning(pThread->GetParameter());

	return Rv;
}

DoliThread::DoliThread()
{
	h_Handle_ = NULL;
	ui32_ThreadID_ = 0;

	pt_Next_ = pt_DoliThread_;
	pt_DoliThread_ = this;

	p_Parameter_ = NULL;

	ui32_Attribute_ = 0;
	ui32_StackSize_ = 0;
	ui_ReturnCode_ = 0;

	c_Name[0] = 0;
}

DoliThread::~DoliThread()
{
	DoliThread * pPrev = NULL;

	pPrev = pt_DoliThread_;
	while ((pPrev != NULL) && (pPrev->pt_Next_ != this))
		pPrev = pPrev->pt_Next_;

	if (pPrev != NULL)
		pPrev->pt_Next_ = pt_Next_;

	if (pt_DoliThread_ == this)
		pt_DoliThread_ = pt_Next_;

	Terminate();
}

bool DoliThread::OnCreate(char * pName, UINT32 iAttr, UINT32 iStackSize, void * UserData)
{
	// Name 세팅
	size_t NameSize = strlen(pName);
	strcpy_s(c_Name, NameSize, pName);

	ui32_Attribute_ = iAttr;
	ui32_StackSize_ = iStackSize;
	
	p_Parameter_ = UserData;

	h_Handle_ = CreateThread(NULL,											// 
							ui32_StackSize_,									// Stack Size	
							(LPTHREAD_START_ROUTINE)_DefaultThreadFunc,		// 기본 함수
							this,											// 함수로 넘어갈 인자값
							ui32_Attribute_,									// Thread의 속성
							(DWORD *)&ui32_ThreadID_);						// Thread 의 ID

	return true;
}

// Thread 멈춤
bool DoliThread::Suspend(void)
{
	UINT32 Rv;
	
	Rv = SuspendThread(h_Handle_);

	// Log 쓰레드 하나 만듭시다.

	return true;
}

// Thread 다시 시작
bool DoliThread::Resume(void)
{
	UINT32 Rv;
	
	Rv = ResumeThread(h_Handle_);

	// 여기도 Log가 필요하다.

	return true;
}

// Thread 종료
bool DoliThread::Terminate(bool bWait)
{
	if (TerminateThread(h_Handle_, ui_ReturnCode_) == FALSE)
	{
		return false;
	}

	if (bWait)
	{
		WaitForSingleObject(h_Handle_, INFINITE);
	}

	return true;
}

UINT32 DoliThread::WaitForFinish(void)
{
	return ::WaitForSingleObject(h_Handle_, INFINITE);
}

void DoliThread::BeforRunning(void * UserData)
{UserData;
	// Thread 시작 전
}

void DoliThread::AfterRunning(void * UserData)
{UserData;
	// Thread 가 끝난다면.
}
