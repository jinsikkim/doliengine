#include "DoliType.h"
#include "DoliNetworkTCPClientSocket.h"
#include "DoliNetworkPacket.h"

DoliNetworkTCPClientSocket::DoliNetworkTCPClientSocket()
{
	s_socket_		= INVALID_SOCKET;
	h_recvEvent_	= INVALID_HANDLE_VALUE;

	i32_receivedPacketSize_ = 0;
	DoliMEM::DOLI_FillZERO(ca_receiveBuffer_, sizeof(ca_receiveBuffer_));
}

DoliNetworkTCPClientSocket::~DoliNetworkTCPClientSocket()
{
	OnDestroy();
}

// Create 는 IP를 어떤 형식으로 받느냐에 따라 달라진다.
bool DoliNetworkTCPClientSocket::OnCreate(char * strIP, UINT16 ui16Port, CallbackRoutine CallBack, void * pAgent)
{
	UINT32 ui32IP = ::inet_addr(strIP);

	return _Create(ui32IP, ui16Port, CallBack, pAgent);
}

bool DoliNetworkTCPClientSocket::OnCreate(UINT32 ui32IP, UINT16 ui16Port, CallbackRoutine CallBack, void * pAgent)
{
	return _Create(ui32IP, ui16Port, CallBack, pAgent);
}

bool DoliNetworkTCPClientSocket::SetSocket(SOCKET Socket)
{
	// 1. 리시브 이벤트 생성
	h_recvEvent_ = ::WSACreateEvent();

	// 2. 소켓 생성
	s_socket_ = Socket;

	// 3. 리시브 관련 멤버 초기화
	i32_receivedPacketSize_ = 0;

	return true;
}

INT32 DoliNetworkTCPClientSocket::OnReceive(void)
{
	// 1. Buffer Size만큼 패킷을 받는다.
	INT32 receivedByte = ::recv(s_socket_, GetRecvBuffer(), PACKETBUFFERSIZE, 0);

	if (SOCKET_ERROR == receivedByte)
	{
		char message[MAX_PATH];
		INT32 error = ::WSAGetLastError();

		sprintf_s(message, "[DoliNetworkTCPClientSocket::OnReceive] lastError = %d", error);
		// 로그를 기록합니다.

		DisConnect();
		return TCP_DISCONNECT_TYPE_READ_ERROR;
	}

	// 2. 받은게 없으면 넘긴다.
	if (0 == receivedByte)
	{
		return receivedByte;
	}

	// 3. 받은 데이터 크기 누적
	i32_receivedPacketSize_ += receivedByte;

	if (CLIENT_SOCKET_BUFFER_SIZE < i32_receivedPacketSize_)
	{
		// Buffer Size를 넘어갔다는 로그 기록
		DisConnect();
		return TCP_DISCONNECT_TYPE_READ_ERROR;
	}

	// 버퍼상의 파싱 시작 위치 
	INT32 i32StartIdx = 0;

	// 4. 수신한 모든 패킷을 파싱한다.
	for (;;)
	{
		// 패킷 사이즈의 버퍼 오버플로우 검사
		INT32 packetSize;
		if (pCB_CallbackFunc_)
		{
			packetSize = (*pCB_CallbackFunc_)(p_Agent_, ca_receiveBuffer_ + i32StartIdx, i32_receivedPacketSize_ - i32StartIdx);
		}
		else
		{
			packetSize = PacketParsing(ca_receiveBuffer_ + i32StartIdx, i32_receivedPacketSize_ - i32StartIdx);
		}

		if (0 > packetSize || PACKETBUFFERSIZE < packetSize)
		{
			// Size 검사에 실패한 로그 기록
			DisConnect();
			return TCP_DISCONNECT_TYPE_READ_ERROR;
		}
		
		// 패킷 파싱이 끝나면 break.
		if (0 == packetSize) break;

		i32StartIdx += packetSize;
		if (i32StartIdx >= i32_receivedPacketSize_)
		{
			break;
		}
	}

	// 5. 파싱 후 남은 데이터를 재정렬한다.
	i32_receivedPacketSize_ -= i32StartIdx;
	if (0 < i32StartIdx && 0 < i32_receivedPacketSize_)
	{
		memmove(ca_receiveBuffer_, ca_receiveBuffer_ + i32StartIdx, i32_receivedPacketSize_);
	}

	return receivedByte;
}

void DoliNetworkTCPClientSocket::OnDestroy(void)
{
	DisConnect();
}

INT32 DoliNetworkTCPClientSocket::PacketParsing(char * pBuffer, INT32 nSize)
{
	pBuffer; nSize;
	// 여기서는 안하고 상속 받고 사용한다.
	return 0;
}

INT32 DoliNetworkTCPClientSocket::SendPacketMessage(DoliNetworkPacket * packet)
{
	// 패킷 클래스에 이미 적용되어 있기 때문에 찾아서 씁니다.
	return SendPacketMessage(packet->GetPacketBuffer(), packet->GetPacketSize());
}

INT32 DoliNetworkTCPClientSocket::SendPacketMessage(const char * pBuffer, INT32 nSize)
{
	// 소켓이 정상적인 소켓이 아닌 경우 보내지 않고 넘깁니다.
	if (INVALID_SOCKET == s_socket_)	return 0;

	// 패킷을 다 보낼때까지 반복한다.
	INT32 sendIdx = 0;
	while (nSize > sendIdx)
	{
		// 1. Send
		INT32 sendedByte = ::send(s_socket_, pBuffer + sendIdx, nSize - sendIdx, 0);

		// 2. 실패 처리 
		if (SOCKET_ERROR == sendedByte)
		{
			char message[MAX_PATH];
			INT32 error = ::WSAGetLastError();

			sprintf_s(message, "[DoliNetworkTCPClientSocket::SendPacketMessage] lastError = %d", error);
			// 로그를 기록한다!

			return -1;
		}

		// 3. 연결이 끊어졌다.
		if (0 == sendedByte)
		{
			return 0;
		}

		// 전송 바이트만큼 인덱스 증가
		sendIdx += sendedByte;
	}

	return INT32();
}

INT32 DoliNetworkTCPClientSocket::SelectEvent(void)
{
	INT32 i32Rv = 0;
	WSANETWORKEVENTS event;
	DoliMEM::DOLI_FillZERO(&event, sizeof(event));

	// 처리할 이벤트를 확인한다.
	if (SOCKET_ERROR != ::WSAEventSelect(s_socket_, h_recvEvent_, FD_WRITE | FD_READ | FD_CLOSE))
	{
		if (SOCKET_ERROR != ::WSAEnumNetworkEvents(s_socket_, h_recvEvent_, &event))
		{
			if (DOLI_ANDBIT(event.lNetworkEvents, FD_READ))
			{
				i32Rv = OnReceive();
			}
			if (DOLI_ANDBIT(event.lNetworkEvents, FD_CLOSE))
			{
				DisConnect();
				i32Rv = TCP_DISCONNECT_TYPE_OTHER;
			}

			return i32Rv;
		}
	}

	// 오류 발생시 처리한다.
	// 오류 발생 원인이 동일하기때문에 출력을 구분하지 않습니다.
	char message[MAX_PATH];
	INT32 error = ::WSAGetLastError();
	sprintf_s(message, "[DoliNetworkTCPClientSocket::SelectEvent] lastError = %d", error);
	// 로그를 기록한다.

	return i32Rv;
}

bool DoliNetworkTCPClientSocket::IsConnected(void) const
{
	if (INVALID_SOCKET == s_socket_)
	{
		return false;
	}

	return true;
}

void DoliNetworkTCPClientSocket::DisConnect()
{
	if (INVALID_HANDLE_VALUE != h_recvEvent_)
	{
		::WSACloseEvent(h_recvEvent_);
		h_recvEvent_ = INVALID_HANDLE_VALUE;
	}

	if (INVALID_SOCKET != s_socket_)
	{
		::shutdown(s_socket_, SD_BOTH);
		::closesocket(s_socket_);
		s_socket_ = INVALID_SOCKET;
	}

	i32_receivedPacketSize_ = 0;
}

SOCKET DoliNetworkTCPClientSocket::GetSocket(void)
{
	return s_socket_;
}

char * DoliNetworkTCPClientSocket::GetRecvBuffer()
{
	return ca_receiveBuffer_ + i32_receivedPacketSize_;
}

bool DoliNetworkTCPClientSocket::_Create(UINT32 ui32IP, UINT16 ui16Port, CallbackRoutine CallBack, void * pAgent)
{
	// 1. 리시브 이벤트 생성
	h_recvEvent_ = ::WSACreateEvent();

	// 2. 소켓 생성
	// TCP로 설정
	s_socket_ = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (INVALID_SOCKET == s_socket_)
	{
		// 실패 로그 기록
		return false;
	}

	// 3. 접속 구조체 생성
	struct sockaddr_in serverAddr;
	DoliMEM::DOLI_FillZERO(&serverAddr, sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = ui32IP;
	serverAddr.sin_port = ::htons(ui16Port);

	// 4. 연결하기
	INT32 result = ::connect(s_socket_, (sockaddr*)&serverAddr, sizeof(serverAddr));

	if (SOCKET_ERROR == result)
	{
		// 소켓 커넥 에러 로그 기록
		return false;
	}

	p_Agent_ = pAgent;
	pCB_CallbackFunc_ = CallBack;

	// 리시브 관련 멤버 초기화
	i32_receivedPacketSize_ = 0;

	return true;
}
