#include "DoliType.h"
#include "DoliMacro.h"

static char tempstr[2048];
static wchar_t tempwstr[2048];

DOLI_EXPORT void DOLIPRINTF(const char * format, ...)
{
	int kk = 0;
	kk;

	va_list marker;

	va_start(marker, format);

	vsprintf_s(tempstr, format, marker);

	va_end(marker);

	OutputDebugString(tempstr);
}

DOLI_EXPORT void DOLIPRINTF(const WCHAR16 * format, ...)
{
	va_list marker;

	va_start(marker, format);

	vswprintf_s(tempwstr, format, marker);

	va_end(marker);

	OutputDebugStringW(tempwstr);
}
