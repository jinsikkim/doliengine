#pragma once
#include "DoliMacro.h"

namespace DNF
{
	DOLI_EXPORT	bool		ServerInit();
	DOLI_EXPORT void		Destroy();

	DOLI_EXPORT char*		GetIPToLongA(UINT32 ui32IP, char* strIP);
	DOLI_EXPORT wchar_t*	GetIPToLongW(UINT32 ui32IP, wchar_t* wstrIP);
	DOLI_EXPORT char*		GetIPToSockA(sockaddr_in* pSockAddr, char* strIP);
	DOLI_EXPORT wchar_t*	GetIPToSockW(sockaddr_in* pSockAddr, wchar_t* wstrIP);
}