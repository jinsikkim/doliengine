#pragma once

#include "DoliMacro.h"

template<class T>
class DOLI_EXPORT DOLI_AFREE
{
public:
	DOLI_AFREE() { _data = nullptr; }
	virtual ~DOLI_AFREE() {}

public:
	void* operator new(size_t size)
	{
		_data = static_cast<T*>(Concurrency::Alloc(size));
		new(_data) T;
		return static_cast<void*>(_data);
	}
	void operator delete(void *p)
	{
		static_cast<T*>(p)->~T();
		return Concurrency::Free(p);
	}

public:
	T* _data;
};