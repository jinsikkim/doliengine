#include "DoliType.h"
#include "DoliNetFunction.h"

DOLI_EXPORT 
bool DNF::ServerInit()
{
	WSADATA wsaData;
	if (::WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		// WSAStart Up 실패 로그 기록
		return false;
	}

	if (wsaData.wVersion != MAKEWORD(2, 2))
	{
		::WSACleanup();

		// WSA Version Fail 로그 기록
		return false;
	}

	return true;
}

DOLI_EXPORT 
void DNF::Destroy()
{
	::WSACleanup();
}

DOLI_EXPORT
char * DNF::GetIPToLongA(UINT32 ui32IP, char * strIP)
{
	INT32 i32Count = 0;
	INT32 i32Count2;
	UINT32 ui32Temp;
	char strTemp[4];

	for (INT32 i = 0; i < 4; i++)
	{
		ui32Temp = (ui32IP >> (8 * i)) & 0x000000FF;

		i32Count2 = 0;
		do
		{
			strTemp[i32Count2] = (ui32Temp % 10) + L'0';
			ui32Temp /= 10;
			i32Count2++;
		} while (0 < ui32Temp);

		do
		{
			i32Count2--;
			strIP[i32Count] = strTemp[i32Count2];
			i32Count++;
		} while (0 < i32Count2);
		strIP[i32Count] = '.';
		i32Count++;
	}
	strIP[i32Count - 1] = L'\0';

	return strIP;
}

DOLI_EXPORT
wchar_t * DNF::GetIPToLongW(UINT32 ui32IP, wchar_t * wstrIP)
{
	INT32 i32Count = 0;
	INT32 i32Count2;
	UINT32 ui32Temp;
	wchar_t wstrTemp[4];

	for (INT32 i = 0; i < 4; i++)
	{
		ui32Temp = (ui32IP >> (8 * i)) & 0x000000FF;

		i32Count2 = 0;
		do
		{
			wstrTemp[i32Count2] = (ui32Temp % 10) + L'0';
			ui32Temp /= 10;
			i32Count2++;
		} while (0 < ui32Temp);

		do
		{
			i32Count2--;
			wstrIP[i32Count] = wstrTemp[i32Count2];
			i32Count++;
		} while (0 < i32Count2);
		wstrIP[i32Count] = '.';
		i32Count++;
	}
	wstrIP[i32Count - 1] = L'\0';

	return wstrIP;
}

DOLI_EXPORT
char * DNF::GetIPToSockA(sockaddr_in * pSockAddr, char * strIP)
{
	return GetIPToLongA(pSockAddr->sin_addr.S_un.S_addr, strIP);
}

DOLI_EXPORT
wchar_t * DNF::GetIPToSockW(sockaddr_in * pSockAddr, wchar_t * wstrIP)
{
	return GetIPToLongW(pSockAddr->sin_addr.S_un.S_addr, wstrIP);
}
