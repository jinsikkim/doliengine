#include "DoliType.h"
#include "DoliNetworkServerSocket.h"

DoliNetworkServerSocket::DoliNetworkServerSocket()
{
	socket_ = INVALID_SOCKET;
}

DoliNetworkServerSocket::~DoliNetworkServerSocket()
{
}

bool DoliNetworkServerSocket::OnCreate(UINT32 ipNO, UINT16 portNO, int backlogCount)
{
	struct sockaddr_in sockAddr;

	DoliMEM::DOLI_FillZERO(&sockAddr, sizeof(sockAddr));
	sockAddr.sin_family			= AF_INET;
	sockAddr.sin_addr.s_addr	= ipNO;
	sockAddr.sin_port			= ::htons(portNO);

	// 1. Set Socket 
	socket_ = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (socket_ == INVALID_SOCKET)
	{
		// Log System 생성후 로그 기록
		return false;
	}

	// 2. Sets a socket option
	INT32 reuse = 1;
	::setsockopt(socket_, SOL_SOCKET, SO_REUSEADDR, (char*)&reuse, sizeof(reuse));

	// 3. Controls the I/O mode of a socket 비동기식으로 설정 
	unsigned long argp = 1;
	::ioctlsocket(socket_, FIONBIO, &argp);

	// 4. Bind Socket 
	if (::bind(socket_, (struct sockaddr *)&sockAddr, sizeof(sockAddr)) == SOCKET_ERROR)
	{
		// Bind 실패 로그
		return false;
	}

	// 5. Listen Socket
	if (::listen(socket_, backlogCount) == SOCKET_ERROR)
	{
		// Listen 실패 로그
		return false;
	}

	// 여기까지 오면 Create 성공
	return true;
}

void DoliNetworkServerSocket::OnDestory()
{
	// 없는 소켓이 아니면 소켓을 지웁니다.
	if (socket_ != INVALID_SOCKET)
		::closesocket(socket_);
	socket_ = INVALID_SOCKET;
}

#pragma warning(push)
#pragma warning(disable: 4127)

SOCKET DoliNetworkServerSocket::AcceptConnection(timeval * ptimeout, sockaddr_in * pAddr, INT32 * pSize)
{
	SOCKET AcceptSocket = INVALID_SOCKET;

	// 1. fd_set 생성
	fd_set fds;
	FD_ZERO(&fds);
	FD_SET(socket_, &fds);

	// 2. select
	if (::select(0, &fds, NULL, NULL, ptimeout) == SOCKET_ERROR)
	{
		return AcceptSocket;
	}

	// 3. 내용이 있을 경우 Accept 시도
	if (FD_ISSET(socket_, &fds))
	{
		AcceptSocket = ::accept(socket_, (struct sockaddr *)pAddr, pSize);
	}

	return  AcceptSocket;
}

#pragma warning(pop)