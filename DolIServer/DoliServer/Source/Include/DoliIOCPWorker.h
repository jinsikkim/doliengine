#pragma once

#include "DoliThread.h"

class DoliNetworkSessionManager;
class DoliNetworkSession;
class DoliCompletionHandler;

class DOLI_EXPORT DoliIOCPWorker : public DoliThread
{
public:
	DoliIOCPWorker();
	virtual ~DoliIOCPWorker();

	virtual void	BeforRunning(void* UserData);
	virtual UINT32	OnRunning(void* UserData);
	virtual void	AfterRunning(void* UserData);

public:
	void SetIocpHandle(HANDLE IocpHandle, DoliNetworkSessionManager * pSessionManager, INT32 ThreadIdx, DoliCompletionHandler *	pCompletionHandler)
	{
		ph_IOCP_				= IocpHandle;
		p_SessionManager_		= pSessionManager;
		i32_Idx_				= ThreadIdx;
		p_CompletionHandler_	= pCompletionHandler;
	}

private:
	HANDLE			ph_IOCP_;

	INT32			i32_Idx_;
	bool			b_IsRun_;
	
	DoliNetworkSessionManager *	p_SessionManager_;
	DoliNetworkSession **		pp_WorkSession_;
	DoliCompletionHandler *		p_CompletionHandler_;	
};