#pragma once

#define SAFE_DELETE(p) { if(p != NULL) delete p; p = NULL; }
#define SAFE_DELETE_ARRAY(p) { if(p != NULL) delete[] p; p = NULL; }

#include <iostream>
#include <string>

inline
void	stdMessage(std::string string)
{
	std::cout << string << std::endl;
}