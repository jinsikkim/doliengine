#include "pch.h"
#include "ServerModule.h"
#include "Defined.h"

ServerModule::ServerModule()
{
	stdMessage("ServerModule constructor");
	memset(&ClientAddr_, 0, sizeof(ClientAddr_));
}

ServerModule::~ServerModule()
{
	stdMessage("ServerModule Destructor");
	Destroy();
}

bool ServerModule::Create()
{
	stdMessage("ServerModule Create");

	// Winsocket은 StartUp 부터 시작
	int result = WSAStartup(MAKEWORD(2, 2), &WsaData_);
	if (result != 0)
	{
		stdMessage("WSAStartUp Error");
		return false;
	}

	ServerSock_ = socket(PF_INET, SOCK_STREAM, 0);
	if (ServerSock_ == INVALID_SOCKET)
	{
		stdMessage("WSAStartUp Error");
		return false;
	}

	// IP File을 오픈합니다.
	_IPFileOpen();

	// Socket Bind
	if (bind(ServerSock_, (SOCKADDR*)&ServerAddr_, sizeof(ServerAddr_)) == SOCKET_ERROR)
	{
		stdMessage("Bind Error");
		return false;
	}

	// Sorket Listen
	if (listen(ServerSock_, 5) == SOCKET_ERROR)
	{
		stdMessage("Listen Error");
		return false;
	}

	// 성공
	return true;
}

void ServerModule::Destroy()
{
	stdMessage("ServerModule Destroy");

	closesocket(ServerSock_);
	closesocket(ClientSock_);
	// 소켓 종료
	WSACleanup();
}

void ServerModule::Start()
{
	int clientaddrsize = sizeof(ClientAddr_);
	ClientSock_ = accept(ServerSock_, (SOCKADDR*)&ClientAddr_, &clientaddrsize);
	if (ClientSock_ == INVALID_SOCKET)
	{
		stdMessage("Accept Error");
	}

	_SendMessage();
}

bool ServerModule::_IPFileOpen()
{
	// 여기서 파일로 IP를 읽는다. 우선 하드 코딩
	memset(&ServerAddr_, 0, sizeof(ServerAddr_));
	ServerAddr_.sin_family = AF_INET;
	ServerAddr_.sin_addr.s_addr = htonl(INADDR_ANY);
	ServerAddr_.sin_port = htons(1214);

	return false;
}

void ServerModule::_SendMessage()
{
	char Message[255] = "Server Connet\0";
	send(ClientSock_, Message, sizeof(Message), 0);
}
