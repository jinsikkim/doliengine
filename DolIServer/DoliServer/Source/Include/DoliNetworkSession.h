#pragma once

//#include "DoliAFree.h"
#include "DoliMacro.h"

#define  SESSION_ASYNCFLAG_SEND		0x01
#define  SESSION_ASYNCFLAG_RECEIVE	0x02
#define	 STRING_SIZE_IPADDRESS		32

struct DOLI_EXPORT OVERLAPPED2 : OVERLAPPED
{
	DWORD flags;
};

class DoliNetworkPacket;

class DOLI_EXPORT DoliNetworkSession //: public DOLI_AFREE<DoliNetworkSession>
{
public:
	DoliNetworkSession();
	virtual ~DoliNetworkSession();

protected:
	void					DispatchReceive(DWORD Transferbytes);

public:
	virtual bool			OnCreate();
	virtual void			OnDestory();

	virtual bool			OnConnect(SOCKET Socket, struct sockaddr_in * pAddr);
	virtual bool			OnConnectInit();
	virtual bool			OnDisconnect(bool bForceMainThread = false);

	void					Dispatch(DWORD Transferbytes);
	bool					SendMessage(DoliNetworkPacket * pPacket);

	bool					GetIsActive(void) { return b_IsActive_; }
	char *					GetIPStringA(void) { return str_IPString_; }
	wchar_t *				GetIPStringW(void) { return wstr_IPString_; }

	virtual INT32			PacketParsing(char * pPacket, INT32 iSize) { pPacket; return iSize; }
	virtual	bool			WaitPacketReceive(INT32 i32Idx);


	virtual bool			SendPacketMessage(DoliNetworkPacket * pPacket);

	// 암호화는 다음에 생각해봅시다.
	//void					DataEncript(UINT32 ui32Key1, UINT32 ui32Key2, UINT64* pui64Value);
	//void					DataDecript(UINT64 ui64Value, UINT32* pui32Key1, UINT32* pui32Key2);

public:
	INT32					i32_SessionIdx_;

	char					str_IPString_[STRING_SIZE_IPADDRESS];
	wchar_t					wstr_IPString_[STRING_SIZE_IPADDRESS];
	UINT32					ui32_ConnectIp_;
	UINT16					ui32_ConnectPort_;

	// Work Thread 
	INT32					i32_WorkThreadIdx_;

protected:
	char					ca_ReceiveBuffer_[PACKETBUFFERSIZE];
	INT32					i32_ReceiveSize_;

private:	
	OVERLAPPED2				OverlappedSend_;
	OVERLAPPED2				OverlappedRecv_;
	SOCKET					Socket_;
	bool					b_IsActive_;
};