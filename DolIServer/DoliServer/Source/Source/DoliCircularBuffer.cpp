#include "DoliType.h"
#include "DoliCircularBuffer.h"
#include "DoliMacro.h"

// UINT 최대 어림잡은 크기
#define CIRCULAR_BUFFER_MAX_INT_COUNT			4000000000

DoliCircularBuffer::DoliCircularBuffer()
{
	cp_Buffer_			= nullptr;
	ui32_BufferSize_	= 0;
	ui32_BufferCount_	= 0;

	ui32_WriteIdx_		= 0;
	ui32_ReadIdx_		= 0;
}

DoliCircularBuffer::~DoliCircularBuffer()
{
	Destroy();
}

bool DoliCircularBuffer::Create(INT32 i32BufferSize, INT32 i32BufferCount)
{
	// 1. size와 Count 기록
	ui32_BufferSize_	= i32BufferSize;
	ui32_BufferCount_	= i32BufferCount;

	// 2. Index 초기화
	ui32_ReadIdx_	= 0;
	ui32_WriteIdx_	= 0;

	// 3. Buffer 할당
	cp_Buffer_ = static_cast<char*>(DoliMEM::DOLI_MEM_ALLOC(ui32_BufferSize_ * ui32_BufferCount_));
	if (nullptr == cp_Buffer_)
		return false;

	return true;
}

void DoliCircularBuffer::Destroy(void)
{
	DOLI_SAFE_FREE(cp_Buffer_);
}

INT32 DoliCircularBuffer::GetEmptyBufferCount(void)
{
	return ui32_BufferCount_ - GetBufferCount();
}

INT32 DoliCircularBuffer::GetBufferCount(void)
{
	INT32 Rv = 0;
	// 내가 읽은 Index보다 Write Index가 클 경우는
	// 사용하고 있는 Buffer가 있다는 뜻.
	if (ui32_WriteIdx_ > ui32_ReadIdx_)
	{
		Rv = ui32_WriteIdx_ - ui32_ReadIdx_;
	}
	return Rv;
}

INT32 DoliCircularBuffer::Push(void * pBuffer)
{
	// Write Index가 UINT 최대 크기에 도달했으면
	// Circular 처럼 맨 앞으로 돌립니다.
	if (ui32_WriteIdx_ > (CIRCULAR_BUFFER_MAX_INT_COUNT - 100))
	{
		if (ui32_WriteIdx_ == ui32_ReadIdx_)
		{
			ui32_WriteIdx_ = 0;
			ui32_ReadIdx_ = 0;
		}
	}

	// Read Index가 다 안읽어서 Index가 없다
	if (ui32_ReadIdx_ > CIRCULAR_BUFFER_MAX_INT_COUNT)
		return CIRCULAR_BUFFER_ERROR_BUFFER_FULLIDX;

	// Buffer의 차이를 비교했는데 최대 Buffer 크기면 Buffer가 없다.
	if ((ui32_WriteIdx_ - ui32_ReadIdx_) >= ui32_BufferCount_)
		return CIRCULAR_BUFFER_ERROR_BUFFER_FULL;

	// Index를 찾는다.
	INT32 iWriteIdx = ui32_WriteIdx_ % ui32_BufferCount_;

	// Buffer 기록
	DoliMEM::DOLI_MEMCOPY(&cp_Buffer_[iWriteIdx * ui32_BufferSize_], pBuffer, ui32_BufferSize_);

	// Index 증가
	ui32_WriteIdx_++;

	return CIRCULAR_BUFFER_SUCCESS;
}

void * DoliCircularBuffer::PushPointer(void)
{
	// Write Index가 UINT 최대 크기에 도달했으면
	// Circular 처럼 맨 앞으로 돌립니다.
	if (ui32_WriteIdx_ > (CIRCULAR_BUFFER_MAX_INT_COUNT - 100))
	{
		if (ui32_WriteIdx_ == ui32_ReadIdx_)
		{
			ui32_WriteIdx_ = 0;
			ui32_ReadIdx_ = 0;
		}
	}

	// Read Index가 다 안읽어서 Index가 없다
	if (ui32_ReadIdx_ > CIRCULAR_BUFFER_MAX_INT_COUNT)
		return nullptr;

	// Buffer의 차이를 비교했는데 최대 Buffer 크기면 Buffer가 없다.
	if ((ui32_WriteIdx_ - ui32_ReadIdx_) >= ui32_BufferCount_)
		return nullptr;

	// Index를 찾는다.
	INT32 iWriteIdx = ui32_WriteIdx_ % ui32_BufferCount_;

	return &cp_Buffer_[iWriteIdx * ui32_BufferSize_];
}

void DoliCircularBuffer::PushPointerIdx(void)
{
	ui32_WriteIdx_++;
}

void * DoliCircularBuffer::Pop(void)
{
	void * pBuffer = nullptr;

	// Buffer가 있다면!
	if (GetBufferCount() > 0)
	{
		// Read 할 Index를 찾는다.
		INT32 iReadIdx = ui32_ReadIdx_ % ui32_BufferCount_;

		// 그 위치의 Buffer를 반환한다.
		pBuffer = &cp_Buffer_[iReadIdx * ui32_BufferSize_];
	}

	return pBuffer;
}

void DoliCircularBuffer::PopIdx(void)
{
	// Read 할 Buffer가 있다면 이미 읽었기 때문에 Index를 증가한다.
	if (GetBufferCount() > 0)
		ui32_ReadIdx_++;
}
