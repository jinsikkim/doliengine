#include "DoliType.h"
#include "DoliNetworkSession.h"
#include "DoliNetworkPacket.h"
#include "DoliNetFunction.h"

DoliNetworkSession::DoliNetworkSession()
{
	b_IsActive_			= FALSE;
	Socket_				= INVALID_SOCKET;
	str_IPString_[0]	= 0;
	wstr_IPString_[0]	= 0;
	i32_WorkThreadIdx_	= -1;
	i32_ReceiveSize_	= 0;

	ui32_ConnectIp_		= 0;
	ui32_ConnectPort_	= 0;

	i32_SessionIdx_		= 0;
}

DoliNetworkSession::~DoliNetworkSession()
{
	OnDestory();
}

void DoliNetworkSession::DispatchReceive(DWORD Transferbytes)
{
	i32_ReceiveSize_ = i32_ReceiveSize_ + Transferbytes;

	// 1. 패킷 사이즈가 버퍼보다 클 경우
	// 없게 만들어야 함.
	if (i32_ReceiveSize_ >= PACKETBUFFERSIZE)
		return;

	// 2. 패킷 버퍼로 파싱하기
	INT32 iReadSize = 0;
	INT32 i32PacketRv;
	while (((i32_ReceiveSize_ - iReadSize) >= PACKETHEADERSIZE))
	{
		i32PacketRv = PacketParsing(&ca_ReceiveBuffer_[iReadSize], (i32_ReceiveSize_ - iReadSize));
		if (0 == i32PacketRv) break;
		iReadSize = iReadSize + i32PacketRv;
	}

	// 3. 받은 패킷보다 더 읽으면 안됨.
	if (iReadSize > i32_ReceiveSize_)
	{
		i32_ReceiveSize_ = 0;
		return;
	}

	// 4. 패킷 읽은 만큼 다시 돌리기
	i32_ReceiveSize_ = i32_ReceiveSize_ - iReadSize;

	// 5. 받은 만큼 처리 못했으면 다시 위치 복귀
	if (0 != i32_ReceiveSize_)
	{
		memmove(ca_ReceiveBuffer_, ca_ReceiveBuffer_ + iReadSize, i32_ReceiveSize_);
	}
	return;
}

bool DoliNetworkSession::OnCreate()
{
	return true;
}

void DoliNetworkSession::OnDestory()
{
	OnDisconnect();
}

bool DoliNetworkSession::OnConnect(SOCKET Socket, sockaddr_in * pAddr)
{
	Socket_ = Socket;

	// IP를 String으로 뽑아낸다.
	DNF::GetIPToSockA(pAddr, str_IPString_);
	DNF::GetIPToSockW(pAddr, wstr_IPString_);

	ui32_ConnectIp_		= pAddr->sin_addr.s_addr;
	ui32_ConnectPort_	= pAddr->sin_port;

	b_IsActive_			= true;

	i32_ReceiveSize_	= 0;

	return true;
}

bool DoliNetworkSession::OnConnectInit()
{
	WaitPacketReceive(-1);
	return true;
}

bool DoliNetworkSession::OnDisconnect(bool bForceMainThread)
{
	bForceMainThread;
	str_IPString_[0]	= '\0';
	wstr_IPString_[0]	= '\0';
	if (Socket_ != INVALID_SOCKET)
	{
		::shutdown(Socket_, SD_BOTH);
		::closesocket(Socket_);
		Socket_ = INVALID_SOCKET;
	}

	b_IsActive_ = FALSE;

	return true;
}

void DoliNetworkSession::Dispatch(DWORD Transferbytes)
{
	if (Transferbytes == 0)return;

	DispatchReceive(Transferbytes);
}

bool DoliNetworkSession::SendMessage(DoliNetworkPacket * pPacket)
{
	if (b_IsActive_ == false) return false;
	if (Socket_ == INVALID_SOCKET) return false;

	DWORD writtenBytes = 0;

	// Overplapped Flag 세팅
	DoliMEM::DOLI_FillZERO(&OverlappedSend_, sizeof(OVERLAPPED2));
	OverlappedSend_.flags = SESSION_ASYNCFLAG_SEND;

	if (!::WriteFile((HANDLE)Socket_, pPacket->GetPacketBuffer(), pPacket->GetPacketSize(), &writtenBytes, (LPOVERLAPPED)&OverlappedSend_))
	{
		DWORD lastError = ::GetLastError();
		if (lastError != ERROR_IO_PENDING && lastError != ERROR_SUCCESS)
		{
			// Send 실패 Log
			return false;
		}
	}

	return true;
}

bool DoliNetworkSession::WaitPacketReceive(INT32 i32Idx)
{
	i32Idx;
	if (Socket_ == INVALID_SOCKET) return false;
	
	DWORD readBytes = 0;

	// Overplapped Flag 세팅
	DoliMEM::DOLI_FillZERO(&OverlappedRecv_, sizeof(OVERLAPPED2));
	OverlappedRecv_.flags = SESSION_ASYNCFLAG_RECEIVE;

	if (!::ReadFile((HANDLE)Socket_, &ca_ReceiveBuffer_[i32_ReceiveSize_], (PACKETBUFFERSIZE - (i32_ReceiveSize_)), &readBytes, (LPOVERLAPPED)&OverlappedRecv_))
	{
		DWORD lastError = ::GetLastError();
		if (lastError != ERROR_IO_PENDING && lastError != ERROR_SUCCESS)
		{
			// 실패 Log
			return false;
		}
	}

	return true;
}

bool DoliNetworkSession::SendPacketMessage(DoliNetworkPacket * pPacket)
{
	DWORD writtenBytes = 0;

	// Overplapped Flag 세팅
	DoliMEM::DOLI_FillZERO(&OverlappedSend_, sizeof(OVERLAPPED2));
	OverlappedSend_.flags = SESSION_ASYNCFLAG_SEND;

	if (!::WriteFile((HANDLE)Socket_, pPacket->GetPacketBuffer(), pPacket->GetPacketSize(), &writtenBytes, (LPOVERLAPPED)&OverlappedSend_))
	{
		DWORD lastError = ::GetLastError();
		if (lastError != ERROR_IO_PENDING && lastError != ERROR_SUCCESS)
		{
			// Send 실패 Log
			return false;
		}
	}

	return true;
}
